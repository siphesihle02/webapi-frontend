# README #

### This is a test for knowledge using the following concepts ###

* GIT + Bitbucket
* ASP.NET Web API
* MS Test
* frontend framework of your choice
* algorithm knowledge

### How do I get set up? ###

* Ensure Visual Studio 2017 is installed https://www.visualstudio.com/thank-you-downloading-visual-studio/?sku=Community&rel=15&page=workloads#
* Ensure .NET Core 2.0 SDK is installed https://www.microsoft.com/net/download/windows
* Fork this repository onto your own account as a **private** repository and share the repository with the person that sent you the test and perform the rest of the tasks on your forked repository. If you do not know how to fork a repository here is information to help.  https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html
* Clone your forked repository onto your PC via GIT command line or visual studio
* Open the solution in visual studio :D
* You will notice an already scaffolded Web.API project and a Web.API.Tests project with some failing unit tests.

### What must I solve? ###

* You must ensure that the unit tests pass by solving the algorithm in the Web.API project "DivisorController":

* Given the set [1, 1, 1, 3, 5, 9] complete the sequence to 11 numbers
* Get the *n*th instance of the sequence where (*n* divide x) has no remainder

* Create a separate user interface project in the same solution using whatever technology you prefer, for example : ASP.NET WebForms / ASP.NET MVC / AngularJS / Aurelia (single page application frameworks are preferred). The user interface must call the *DivisorController.Get* API method to allow the user to input the 2 required parameters and see the output on the view.

* Using branches is preferrable

* Commits to the repository must be done at regular intervals during development

* Once completed with all the tasks please ensure all the work is completely committed and pushed to the master branch.

* Share the repository with the person that sent you the test